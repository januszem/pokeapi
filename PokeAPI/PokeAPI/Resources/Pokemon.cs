﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PokeAPI.Resources
{
    public class Camera
    {
        public double DiskRadiusM { get; set; }
        public double CylRadiusM { get; set; }
        public double CylHeightM { get; set; }
        public double ShoulderModeScale { get; set; }
        public double? CylGroundM { get; set; }
    }

    public class Encounter
    {
        public double BaseCaptureRate { get; set; }
        public double BaseFleeRate { get; set; }
        public double CollisionRadiusM { get; set; }
        public double CollisionHeightM { get; set; }
        public double CollisionHeadRadiusM { get; set; }
        public string MovementType { get; set; }
        public int MovementTimerS { get; set; }
        public double JumpTimeS { get; set; }
        public int AttackTimerS { get; set; }
    }

    public class Stats
    {
        public int BaseStamina { get; set; }
        public int BaseAttack { get; set; }
        public int BaseDefense { get; set; }
    }

    public class Pokemon
    {
        public string UniqueId { get; set; }
        public double ModelScale { get; set; }
        public string Type1 { get; set; }
        public string Type2 { get; set; }
        public Camera Camera { get; set; }
        public Encounter Encounter { get; set; }
        public Stats Stats { get; set; }
        public string QuickMoves { get; set; }
        public string CinematicMoves { get; set; }
        public string AnimTime { get; set; }
        public string Evolution { get; set; }
        public int EvolutionPips { get; set; }
        public double PokedexHeightM { get; set; }
        public double PokedexWeightKg { get; set; }
        public double HeightStdDev { get; set; }
        public double WeightStdDev { get; set; }
        public object FamilyId { get; set; }
        public int CandyToEvolve { get; set; }
        public string ParentId { get; set; }
        public string PokemonClass { get; set; }
    }

    public class RootObject
    {
        public string TemplateId { get; set; }
        public Pokemon Pokemon { get; set; }
    }
}