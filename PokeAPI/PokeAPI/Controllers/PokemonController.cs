﻿using PokeAPI.Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Script;
using System.Web.Script.Serialization;

namespace PokeAPI.Controllers
{
  
    public class PokemonController : ApiController
    {
        // GET: Pokemon
        public object Get()
        {
            var json_serializer = new JavaScriptSerializer();
            List<string> routes_list = json_serializer.Deserialize<List<string>>(File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/Resources/PokemonNames.json"));
            return routes_list;
        }

        public object Get(string name)
        {
            var json_serializer = new JavaScriptSerializer();
            List<RootObject> routes_list = json_serializer.Deserialize<List<RootObject>>(File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/Resources/Pokemons.json"));

            var pokemon = routes_list.Where(x => x.TemplateId.ToLower().Contains(name.ToLower())).FirstOrDefault();

             return pokemon;
        }
    }

}